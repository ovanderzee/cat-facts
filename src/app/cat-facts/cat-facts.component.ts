import { Component, OnInit } from '@angular/core';
import { FetchService } from '../lib/fetch.service';
import { ICatFact, ICatParams } from "../lib/types";

@Component({
  selector: 'app-cat-facts',
  templateUrl: './cat-facts.component.html',
  styleUrls: ['./cat-facts.component.scss']
})
export class CatFactsComponent implements OnInit {
  cats$!: Promise<ICatFact[]>;
  private params: ICatParams = {animal_type: 'dog', amount: 12};

  constructor(
    private fetchService: FetchService
  ) { }

  ngOnInit(): void {
    this.cats$ = this.fetchService.fetchCats(this.params);
  }
}
