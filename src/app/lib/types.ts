import {stringify} from "@angular/compiler/src/util";

export interface ICatFact {
  used: boolean,
  source: string,
  type: string,
  deleted: boolean,
  _id: string,
  __v: number,
  text: string,
  updatedAt: string,
  createdAt: string,
  status: any,
  user: string
}

export interface ICatParams {
  animal_type?: string,
  amount?: number
}
