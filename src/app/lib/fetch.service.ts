import { Injectable } from '@angular/core';
import { ICatFact, ICatParams } from "./types";

const ENDPOINT: string = 'https://cat-fact.herokuapp.com/facts/random';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  private literalToQueryString (literal: ICatParams): string {
    const pairs = Object.entries(literal)
      .map((e: [string, any]) => encodeURIComponent(e[0]) + '=' + encodeURIComponent(e[1]));
    return pairs.join('&');
  }

  public fetchCats(literal: ICatParams): Promise<ICatFact[]> {
    const factsUrl = ENDPOINT + '?' + this.literalToQueryString(literal);

    return fetch(factsUrl)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .then(list => {
        if (!list.length) {
          throw new Error('Empty response');
        }
        return list;
      })
      .catch(error => {
        return [{
            type: literal.animal_type,
            deleted: false,
            _id: 'error',
            text: error.message,
          } as ICatFact];
      })
    ;
  }

}
